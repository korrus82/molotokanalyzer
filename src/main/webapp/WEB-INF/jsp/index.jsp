<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="http://getbootstrap.com/favicon.ico">
	<link href="webjars/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	<link href="webjars/bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<title>Molotok Analyzer</title>
	<script src="webjars/jquery/2.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>
	<script src="webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="js/molotok.js"></script>
</head>
<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-default navbar-static-top" role="navigation">
		<!-- 	<div class="navbar navbar-inverse navbar-static-top" role="navigation"> -->
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="?">Analyzer</a>
			</div>
			<div class="navbar-collapse">
				<c:if test="${isAuthorized}">
					<ul class="nav navbar-nav">
						<li>
							<form class="navbar-form" role="search">
								<!-- 							<div class="form-group"> -->
								<select name="search_scope" class="form-control">
									<option <c:if test="${searchScope == '0'}">selected</c:if> value="0">все разделы</option>
									<c:forEach var="each" items="${categoriesList}">
										<option <c:if test="${searchScope == each.getCategoryId()}"> selected</c:if> value="${each.getCategoryId()}">${each.getTitle()}</option>
									</c:forEach>
								</select>
								<div class="input-group">
									<input type="text" placeholder="Search" class="form-control" name="search" value="${search}">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-search"></span>
									</div>
								</div>
							</form>
						</li>
					</ul>

					<%-- 				<c:if test="${isAuthorized}"> --%>
					<ul class="nav pull-right">
						<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<button type="button" class="btn btn-default">
									${user.getEmail()} <b class="caret"></b>
								</button>
						</a>
							<div class="dropdown-menu" style="padding: 0px; min-width: 300px;">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">User info</h3>
									</div>
									<div class="panel-body">


										<table class="table">
											<tbody>
												<tr>
													<td>Name:</td>
													<td>${user.getName()}</td>
												</tr>
												<tr>
													<td>Lastname:</td>
													<td>${user.getLastname()}</td>
												</tr>
												<tr>
													<td>Gender:</td>
													<td>${user.getGender()}</td>
												</tr>
												<tr>
													<td>Country:</td>
													<td>${user.getCountry().getName()}</td>
												</tr>
												<tr>
													<td>Region:</td>
													<td>${user.getRegion().getName()}</td>
												</tr>
												<tr>
													<td>City:</td>
													<td>${user.getCity().getName()}</td>
												</tr>
											</tbody>
										</table>
										<form class="navbar-form" role="form">
											<input name="logout" type="submit" class="btn btn-danger" value="Logout">
										</form>
									</div>
								</div>
							</div></li>
					</ul>
				</c:if>


				<c:if test="${!isAuthorized}">
					<ul class="nav pull-right">
						<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<button type="button" class="btn btn-default">
									Login <b class="caret"></b>
								</button>
						</a>
							<div class="dropdown-menu" style="padding: 0px; min-width: 300px;">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Please sign in</h3>
									</div>
									<div class="panel-body">
										<form id="loginform" method="post" accept-charset="UTF-8">
											<fieldset>
												<div class="form-group">

													<input class="form-control" placeholder="E-mail" name="email" id="email" type="text">
												</div>
												<div class="form-group">
													<input class="form-control" placeholder="Password" name="password" id="password" type="password" value="">
												</div>
												<div class="checkbox">
													<label> <input name="remember" type="checkbox" value="Remember Me"> Remember Me
													</label>
												</div>
											</fieldset>
											<input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
										</form>
									</div>
								</div>
							</div></li>
					</ul>
					<ul class="nav pull-right">
						<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<button type="button" class="btn btn-default">
									Sign Up <b class="caret"></b>
								</button>
						</a>
							<div class="dropdown-menu" style="padding: 0px; min-width: 300px;">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Registration</h3>
									</div>
									<div class="panel-body">
										<form action="registration.action" id="registerform" method="post" accept-charset="UTF-8">
											<fieldset>
												<div class="form-group">
													<input class="form-control" placeholder="Name" name="name" id="name" type="text">
												</div>
												<div class="form-group">
													<input class="form-control" placeholder="Last Name" name="lastname" id="lastname" type="text">
												</div>
												<div class="form-group">
													<select name="gender" id="gender" class="form-control">
														<option value="Male">Male</option>
														<option value="Female">Female</option>
													</select>
												</div>
												<div class="form-group">
													<input class="form-control" placeholder="E-mail" name="email" id="email" type="text">
												</div>
												<div class="form-group">
													<input class="form-control" placeholder="Password" name="password" id="password" type="password" value="">
												</div>
												<div class="form-group">
													<label>Country<select name="country" id="country" class="form-control">
															<c:forEach var="each" items="${countryList}">
																<option value="${each.getCountryId()}">${each.getName()}</option>
															</c:forEach>
													</select>
													</label>
												</div>
												<div class="form-group">
													<label>Region<select name="region" id="region" class="form-control">
													</select>
													</label>
												</div>
												<div class="form-group">
													<label>City<select name="city" id="city" class="form-control">
													</select>
													</label>
												</div>
											</fieldset>
											<input class="btn btn-lg btn-success btn-block" type="submit" value="Registration">
										</form>
									</div>
								</div>
							</div></li>
					</ul>
				</c:if>
			</div>
		</div>
	</div>
	<!--/.nav-collapse -->

	<!-- Begin page content -->

	<div class="container bs-docs-container">
		<c:if test="${isAuthorized}">

			<div class="pagination-centered">
				<ul class="pagination">
					<li><a href="?page=1&search=${search}&order=${order}&search_scope=${searchScope}"><span
							class="glyphicon glyphicon-chevron-left"></span></a></li>
					<c:forEach var="each" items="${pageWrapper.getPages()}">
						<c:if test="${each == page}">
							<li class="active"><a href="?page=${each}&search=${search}&order=${order}&search_scope=${searchScope}">${each}<span
									class="sr-only">(current)</span></a></li>
						</c:if>
						<c:if test="${each != page}">
							<li><a href="?page=${each}&search=${search}&order=${order}&search_scope=${searchScope}">${each}</a></li>
						</c:if>
					</c:forEach>
					<li><a href="?page=${pagesCount}&search=${search}&order=${order}&search_scope=${searchScope}"><span
							class="glyphicon glyphicon-chevron-right"></span></a></li>
					<li><span>Total: ${pagesCount}</span></li>
				</ul>
			</div>


			<table class="table table-striped">
				<thead>
					<tr>
						<th><h4></h4></th>
						<th><h4>Название</h4></th>
						<th>

							<h4>
								<a href="?page=${page}&search=${search}&order=pdesc&search_scope=${searchScope}"> <span
									class="glyphicon glyphicon-chevron-up"></span></a> Цена <a
									href="?page=${page}&search=${search}&order=pasc&search_scope=${searchScope}"> <span
									class="glyphicon glyphicon-chevron-down"></span></a>
							</h4>
						</th>
						<th>
							<h4>
								<a href="?page=${page}&search=${search}&order=fdesc&search_scope=${searchScope}"> <span
									class="glyphicon glyphicon-chevron-up"></span></a> Спрос <a
									href="?page=${page}&search=${search}&order=fasc&search_scope=${searchScope}"> <span
									class="glyphicon glyphicon-chevron-down"></span></a>
							</h4>
						</th>
						<th><h4>Количество</h4></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="each" items="${itemsList}">
						<tr>
							<td><img src="${each.getImageUrl()}"></td>
							<td><h5>
									<a href="http://molotok.ru/item${each.getItemId()}_${each.getItemId()}.html">${each.getTitle()}</a>
								</h5></td>
							<td><h5>${each.getPrice()}</h5></td>
							<td><h5>${each.getFrequency()}</h5></td>
							<td><h5>${each.getAmount()}</h5></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<div class="pagination-centered">
				<ul class="pagination">
					<li><a href="?page=1&search=${search}&order=${order}&search_scope=${searchScope}"><span
							class="glyphicon glyphicon-chevron-left"></span></a></li>
					<c:forEach var="each" items="${pageWrapper.getPages()}">
						<c:if test="${each == page}">
							<li class="active"><a href="?page=${each}&search=${search}&order=${order}&search_scope=${searchScope}">${each}<span
									class="sr-only">(current)</span></a></li>
						</c:if>
						<c:if test="${each != page}">
							<li><a href="?page=${each}&search=${search}&order=${order}&search_scope=${searchScope}">${each}</a></li>
						</c:if>
					</c:forEach>
					<li><a href="?page=${pagesCount}&search=${search}&order=${order}&search_scope=${searchScope}"><span
							class="glyphicon glyphicon-chevron-right"></span></a></li>
					<li><span>Total: ${pagesCount}</span></li>
				</ul>
			</div>

		</c:if>
		<c:if test="${!isAuthorized}">
			<div class="jumbotron">
				<!-- 				<h1>Hello, world!</h1> -->
				<p>Добро пожаловать на сайт, пожалуйста зарегистрируйтесь</p>
			</div>
		</c:if>
	</div>

</body>
</html>