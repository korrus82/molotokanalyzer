if (typeof jQuery === 'undefined') {
	throw new Error('Bootstrap\'s JavaScript requires jQuery')
}



+function($) {
	'use strict';
	
$(document).on('click', '#country', function() {
		var countryId = $('#country').val();
		$.getJSON("region-list.action?countryId=" + countryId, function(region) {
		var html = "";
		for (var i = 0; i < region.length; i++) {
			html = html + "<option value=\"" 	+ region[i].regionId + "\">" 	+ region[i].name + "</option> \r\n";
		}
		$("#city").empty();
		$("#region").empty().append(html);
		});
	})

$(document).on('click', '#region', function() {
		var regionId = $('#region').val();
		$.getJSON("city-list.action?regionId=" + regionId, function(city) {
			var html = "";
			for (var i = 0; i < city.length; i++) {
				html = html + "<option value=\"" + city[i].cityId	+ "\">"	+ city[i].name + "</option> \r\n";
			}
			$("#city").empty().append(html);
		});
	})

}(jQuery);

+function($) {
	'use strict';

	// CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
	// ============================================================

	$(document).ready(function() {
		$('#registerform').bootstrapValidator(
				{
					// live : 'enabled',
					message : 'This value is not valid',
					feedbackIcons : {
						valid : 'glyphicon glyphicon-ok',
						invalid : 'glyphicon glyphicon-remove',
						validating : 'glyphicon glyphicon-refresh'
					},
					fields : {
						
						name : {
							message : 'The username is not valid',
							validators : {
								notEmpty : {
									message : 'The username is required and cannot be empty'
								},
								stringLength : {
									min : 6,
									max : 30,
									message : 'The username must be more than 6 and less than 30 characters long'
								},
								regexp : {
									regexp : /^[a-zA-Z0-9_]+$/,
									message : 'The username can only consist of alphabetical, number and underscore'
								}
							}
						},
						lastname : {
							message : 'The lastname is not valid',
							validators : {
								notEmpty : {
									message : 'The lastname is required and cannot be empty'
								},
								stringLength : {
									min : 6,
									max : 30,
									message : 'The lastname must be more than 6 and less than 30 characters long'
								},
								regexp : {
									regexp : /^[a-zA-Z0-9_]+$/,
									message : 'The lastname can only consist of alphabetical, number and underscore'
								}
							}
						},
						email : {
							validators : {
								notEmpty : {
									message : 'The email is required and cannot be empty'
								},
								emailAddress : {
									message : 'The input is not a valid email address'
								}
							}
						},
						password : {
							validators : {
								notEmpty : {
									message : 'The password is required'
								}
							}
						},
						region : {
							validators : {
								notEmpty : {
									message : 'The region is required'
								}
							}
						},
						city : {
							validators : {
								notEmpty : {
									message : 'The city is required'
								}
							}
						}
					}
				});
	});

	$(document).ready(function() {
		$('#loginform').bootstrapValidator(
				{
					// live : 'enabled',
					message : 'This value is not valid',
					feedbackIcons : {
						valid : 'glyphicon glyphicon-ok',
						invalid : 'glyphicon glyphicon-remove',
						validating : 'glyphicon glyphicon-refresh'
					},
					fields : {

						email : {
							validators : {
								notEmpty : {
									message : 'The email is required and cannot be empty'
								},
								emailAddress : {
									message : 'The input is not a valid email address'
								}
							}
						},
						password : {
							validators : {
								notEmpty : {
									message : 'The password is required'
								}
							}
						}
					}
				});
	});

}(jQuery);
