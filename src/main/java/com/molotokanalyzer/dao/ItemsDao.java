package com.molotokanalyzer.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.molotokanalyzer.beans.Item;
@SuppressWarnings("unchecked")
@Repository
public class ItemsDao {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Item> getItemsList(String search, int page, String orderText, int searchScope) {

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Item.class);

        if (searchScope >= 1 && searchScope <= 9) {
            criteria.add(Restrictions.eq("categoryId", searchScope));
        }

        if (search != null) {
            criteria.add(Restrictions.like("title", search, MatchMode.ANYWHERE));
        }

        switch (orderText) {
        case "fasc":
            criteria.addOrder(Order.asc("frequency"));
            break;
        case "pasc":
            criteria.addOrder(Order.asc("price"));
            break;
        case "pdesc":
            criteria.addOrder(Order.desc("price"));
            break;
        default:
            criteria.addOrder(Order.desc("frequency"));
            break;
        }
        criteria.setFirstResult((page - 1) * 50);
        criteria.setMaxResults(50);
        // List<Item> itemsList = criteria.list();
        return criteria.list();
    }

    public int getItemsCount(String search, int searchScope) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Item.class);

        if (searchScope >= 1 && searchScope <= 9) {
            criteria.add(Restrictions.eq("categoryId", searchScope));
        }

        if (search != null) {
            criteria.add(Restrictions.like("title", search, MatchMode.ANYWHERE));
        }
        
        return criteria.list().size();
    }

}
