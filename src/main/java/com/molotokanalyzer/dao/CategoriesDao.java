package com.molotokanalyzer.dao;

import com.molotokanalyzer.beans.Category;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoriesDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    public List<Category> getCategoriesList() {
        return sessionFactory.getCurrentSession().createCriteria(Category.class).list();
    }
}
