package com.molotokanalyzer.dao;

import com.molotokanalyzer.beans.City;
import com.molotokanalyzer.beans.Country;
import com.molotokanalyzer.beans.Region;
import com.molotokanalyzer.beans.User;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unchecked")
@Repository
public class UsersDao {

    @Autowired
    private SessionFactory sessionFactory;

    public String getPasswordHash(String email) {
        User user = (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("email", email)).uniqueResult();
        String passwordHash = null;
        try {
            passwordHash = user.getPassword();
        } catch (NullPointerException ignored) {
        }
        return passwordHash;
    }

    public void saveHash(String email, String hash) {
        User user = (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("email", email)).uniqueResult();
        user.setHash(hash);
        sessionFactory.getCurrentSession().save(user);
        sessionFactory.getCurrentSession().flush();
    }

    public boolean isAuthorized(String hash) {
        return sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("hash", hash)).uniqueResult() != null;
    }

    public void removeHash(String hash) {
        User user = (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("hash", hash)).uniqueResult();
        user.setHash("");
        sessionFactory.getCurrentSession().save(user);
        sessionFactory.getCurrentSession().flush();

        // sessionFactory.getCurrentSession().createQuery("update User set hash = '' where hash = '" + hash +
        // "'").executeUpdate();
    }

    public boolean isExist(String email) {
        return sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("email", email)).uniqueResult() != null;
    }

    public void registration(User user) {
        sessionFactory.getCurrentSession().save(user);
        sessionFactory.getCurrentSession().flush();
    }

    public User getUser(String hash) {
        return (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("hash", hash)).uniqueResult();
    }

    public List<Country> getCountryList() {
        return sessionFactory.getCurrentSession().createCriteria(Country.class).list();
    }

    public List<Region> getRegionList(int countryId) {
        return sessionFactory.getCurrentSession().createCriteria(Region.class).add(Restrictions.eq("countryId", countryId)).list();
    }

    public List<City> getCityList(int regionId) {
        return sessionFactory.getCurrentSession().createCriteria(City.class).add(Restrictions.eq("regionId", regionId)).list();
    }

    public Country getCountry(int countryId) {
        return (Country) sessionFactory.getCurrentSession().createCriteria(Country.class).add(Restrictions.eq("countryId", countryId)).uniqueResult();
    }

    public Region getRegion(int regionId) {
        return (Region) sessionFactory.getCurrentSession().createCriteria(Region.class).add(Restrictions.eq("regionId", regionId)).uniqueResult();
    }

    public City getCity(int cityId) {
        return (City) sessionFactory.getCurrentSession().createCriteria(City.class).add(Restrictions.eq("cityId", cityId)).uniqueResult();
    }

}