package com.molotokanalyzer.controllers;

import com.molotokanalyzer.beans.*;
import com.molotokanalyzer.services.CategoriesService;
import com.molotokanalyzer.services.ItemsService;
import com.molotokanalyzer.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
class MolotokController {
    
    @Autowired
    private CategoriesService categoriesService;
    @Autowired
    private ItemsService itemsService;
    @Autowired
    private UsersService usersService;   

    @RequestMapping(value = "/index.action", method = RequestMethod.GET)
    public ModelAndView getIndex(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "page", required = false, defaultValue="1") int page,
            @RequestParam(value = "order", required = false, defaultValue="fdesc") String orderText,
            @RequestParam(value = "search_scope", required = false, defaultValue="0") int searchScope,
            @RequestParam(value = "logout", required = false) String logout,
            @CookieValue(value = "hash", required = false) String hash) {
        
        ModelAndView mav = new ModelAndView("index");
        boolean isAuthorized = usersService.isAuthorized(hash);
        List<Country> countryList = usersService.getCountryList();
        
        if (isAuthorized) {
            User user = usersService.getUser(hash);

            List<Item> itemsList = itemsService.getItemsList(search, page, orderText, searchScope);
            List<Category> categoriesList = categoriesService.getCategoriesList();
            int pagesCount = itemsService.getPagesCount(search, searchScope);
            PageWrapper pageWrapper = new PageWrapper(pagesCount, page);

            mav.addObject("search", search);
            mav.addObject("itemsList", itemsList);
            mav.addObject("categoriesList", categoriesList);
            mav.addObject("pageWrapper", pageWrapper);
            mav.addObject("pagesCount", pagesCount);
            mav.addObject("page", page);
            mav.addObject("order", orderText);
            mav.addObject("searchScope", searchScope);
            mav.addObject("user", user);
        }
        
        if (isAuthorized && logout != null) {
            if (logout.equals("Logout")) {
                Cookie cookie = usersService.getDeAuthorizationCookie(request.getCookies());
                response.addCookie(cookie);
                isAuthorized = false;
            }
        }
        mav.addObject("isAuthorized", isAuthorized);
        mav.addObject("countryList", countryList);

        return mav;
    }

    @RequestMapping(value = "/index.action", method = RequestMethod.POST)
    public ModelAndView postLogin(HttpServletResponse response,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "password", required = false) String password) {
        
        Cookie cookie = usersService.getAuthorizationCookie(email, password);
        if (cookie != null) {
            response.addCookie(cookie);
        }
        return new ModelAndView("redirect:index.action");
    }

}