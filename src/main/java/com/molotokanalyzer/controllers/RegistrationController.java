package com.molotokanalyzer.controllers;

import com.molotokanalyzer.beans.*;
import com.molotokanalyzer.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.UUID;

@Controller
class RegistrationController {

    @Autowired
    private UsersService usersService;

    @RequestMapping(value = "/registration.action", method = RequestMethod.POST)
    public ModelAndView postRegistration(HttpServletResponse response,
                                         @RequestParam(value = "name", required = true) String name,
                                         @RequestParam(value = "lastname", required = true) String lastname,
                                         @RequestParam(value = "gender", required = true) String gender,
                                         @RequestParam(value = "email", required = true) String email,
                                         @RequestParam(value = "password", required = true) String password,
                                         @RequestParam(value = "country", required = true) int countryId,
                                         @RequestParam(value = "region", required = true) int regionId,
                                         @RequestParam(value = "city", required = true) int cityId) {

        boolean isExist = usersService.isExist(email);
        if (!isExist) {
            try {
                password = PasswordHash.createHash(password);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                e.printStackTrace();
            }

            User user = new User();
            String hash = UUID.randomUUID().toString();

            user.setName(name);
            user.setLastname(lastname);
            user.setGender(gender);
            user.setEmail(email);
            user.setPassword(password);
            user.setHash(hash);

            Country country = usersService.getCountry(countryId);
            user.setCountry(country);
            Region region = usersService.getRegion(regionId);
            user.setRegion(region);
            City city = usersService.getCity(cityId);
            user.setCity(city);

            usersService.registration(user);
            Cookie cookie = new Cookie("hash", hash);
            response.addCookie(cookie);
        }

        return new ModelAndView("redirect:index.action");
    }

    @RequestMapping(value = "/region-list.action", method = RequestMethod.GET, headers = "Accept=application/json", produces = {"application/json; charset=UTF-8"})
    public
    @ResponseBody
    List<Region> getRegionList(@RequestParam(value = "countryId", required = true) int countryId) {
        return usersService.getRegionList(countryId);
    }

    @RequestMapping(value = "/city-list.action", method = RequestMethod.GET, headers = "Accept=application/json", produces = {"application/json; charset=UTF-8"})
    public
    @ResponseBody
    List<City> getCityList(@RequestParam(value = "regionId", required = true) int regionId) {
        return usersService.getCityList(regionId);
    }
}