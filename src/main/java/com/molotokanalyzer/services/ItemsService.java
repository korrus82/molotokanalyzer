package com.molotokanalyzer.services;

import com.molotokanalyzer.beans.Item;
import com.molotokanalyzer.dao.ItemsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItemsService {

    @Autowired
    private ItemsDao itemsDao;

    @Transactional
    public List<Item> getItemsList(String search, int page, String orderText, int searchScope) {
        return itemsDao.getItemsList(search, page, orderText, searchScope);
    }

    @Transactional
    public int getPagesCount(String search, int searchScope) {
        int itemsCount = itemsDao.getItemsCount(search, searchScope);
        return (int) Math.ceil(itemsCount / 50d);
    }
}
