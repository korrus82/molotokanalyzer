package com.molotokanalyzer.services;

import com.molotokanalyzer.beans.*;
import com.molotokanalyzer.dao.UsersDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.UUID;

@Service
public class UsersService {

    @Autowired
    private UsersDao usersDao;

    @Transactional
    public Cookie getAuthorizationCookie(String email, String password) {

        String passwordHash = usersDao.getPasswordHash(email);
        if (passwordHash == null) {
            return null;
        }
        boolean isAuthorized = false;
        try {
            isAuthorized = PasswordHash.validatePassword(password, passwordHash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

        if (isAuthorized) {
            String hash = UUID.randomUUID().toString();
            usersDao.saveHash(email, hash);
            return new Cookie("hash", hash);
        }

        return null;
    }

    @Transactional
    public boolean isAuthorized(String hash) {
        return usersDao.isAuthorized(hash);
    }

    @Transactional
    public Cookie getDeAuthorizationCookie(Cookie[] cookies) {
        for (Cookie each : cookies) {
            if (each.getName().equals("hash")) {
                usersDao.removeHash(each.getValue());
                each.setMaxAge(0);
                each.setValue("");
                return each;
            }
        }
        return null;
    }

    @Transactional
    public List<Country> getCountryList() {
        return usersDao.getCountryList();
    }

    @Transactional
    public List<Region> getRegionList(int countryId) {
        return usersDao.getRegionList(countryId);
    }

    @Transactional
    public List<City> getCityList(int regionId) {
        return usersDao.getCityList(regionId);
    }

    @Transactional
    public boolean isExist(String email) {
        return usersDao.isExist(email);
    }

    @Transactional
    public void registration(User user) {
        usersDao.registration(user);
    }

    @Transactional
    public User getUser(String hash) {
        return usersDao.getUser(hash);
    }

    @Transactional
    public Country getCountry(int countryId) {
        return usersDao.getCountry(countryId);
    }

    @Transactional
    public Region getRegion(int regionId) {
        return usersDao.getRegion(regionId);
    }

    @Transactional
    public City getCity(int cityId) {
        return usersDao.getCity(cityId);
    }

}
