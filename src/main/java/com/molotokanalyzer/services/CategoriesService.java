package com.molotokanalyzer.services;

import com.molotokanalyzer.beans.Category;
import com.molotokanalyzer.dao.CategoriesDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CategoriesService {
    
    @Autowired
    private CategoriesDao categoriesDao;

    @Transactional
    public List<Category> getCategoriesList() {
        return categoriesDao.getCategoriesList();
    }
}
