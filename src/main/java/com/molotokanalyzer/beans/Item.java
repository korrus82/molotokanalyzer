package com.molotokanalyzer.beans;

import javax.persistence.*;

@Entity
@Table(name = "items")
public class Item {
    @Id
    @Column(name = "item_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long itemId;
    
    @Column(name = "title")
    private String title;
    
    @Column(name = "category_id")
    private int categoryId;
    
    @Column(name = "price")
    private int price;
    
    @Column(name = "frequency")
    private int frequency;
    
    @Column(name = "image_url")
    private String imageUrl;
    
    @Column(name = "amount")
    private int amount;

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Item [itemId=" + itemId + ", title=" + title + ", categoryId=" + categoryId + ", price=" + price + ", frequency=" + frequency + ", imageUrl="
                + imageUrl + ", amount=" + amount + "]";
    }
}
