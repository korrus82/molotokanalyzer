package com.molotokanalyzer.beans;

import javax.persistence.*;

@Entity
@Table(name = "city")
public class City {
    @Id
    @Column(name = "city_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int cityId;
    
    @Column(name = "country_id")
    private int countryId;
    
    @Column(name = "region_id")
    private int regionId;
    
    @Column(name = "name")
    private String name;

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "City [regionId=" + regionId + ", countryId=" + countryId + ", cityId=" + cityId + ", name=" + name + "]";
    }

}
